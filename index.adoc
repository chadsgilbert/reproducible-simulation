= Reproducible Simulation
Chad Gilbert <chad.s.gibert@gmail.com>
:toc:
:stem:

Here I walk though some simple studies in epidemiological dynamics by
investigating several different epidemiological models, and anlysing key
dynamics of these models using simulation. In the sections that follow, I will
describe different ways of considering how an infectious disease can propagate
through a population.

== Goals

Although have an amateur interest in understanding epidemiology, the true goal
of this project is to practice and demonstrate a particular methodology for
writing analytical reports based on computer simulation. The challenge with
currently popular workflows for conducting and reporting on technical anlayses
is that it is very easy to lose
track of the process that was used to produce the analysis or report, to lose
the ability to reproduce it, and to make mistakes that can never be recovered,
checked, or revisited. Microsoft's Office desktop productivity suite is the
worst offender, but even workflows using better tools can easily come up short
of reproducibility and re-usability.

There is no literature that I have been able to find that focuses on this kind
of technical research for the Enterprise, though I'm sure many of the practices
I'm trying to learn have been learned in industry before. However, the topic of
reproducible research has recently become popular in academia. Two such books
on the topic that are openly available are
link:https://the-turing-way.netlify.com/[The Turing Way] and
link:https://www.practicereproducibleresearch.org/[The Practice of Reproducible
Research]. Although these books frame the problem in a way that is not exactly
aligned with my own interests, they both highlight many of the same challenges
that I am concerned with, and provide excellent guidance on solving some of the
issues. I owe several of the lessons used here to those books.

== General Approach

The simplest way to ensure an analysis is reproducible is to frequently try to
reproduce it from scratch. And the only way to make that acceptable is to
automate it. So the general idea is very simple: we need to automate the
simulations, automate post-processing of the simulations to produce figures and
tables, and automate production of the document.

Even with the process automated, there are a lot of variables that can thwart
attempts to reproduce an analysis. The result can change or the software can
just plain fail unless the same versions of the data, content, software,
operating system and sometimes even hardware are different. There are two
possible approaches to supporting a reproduction: one, make it possible to
obtain the exact same configuration, or two make the analysis such that it works
the same on other configurations. The particular solution to the problem is
different for each of the different kinds of input.

=== Data

If data are used which cannot be reproduced, they should be stored somewhere
known, where there is a single source of truth for it, and it should be
immutable. There are internet archives designed for managing this, as well as
specialized tools such as link:https://dvc.org[DVC], which claim to solve this
problem. I believe in some cases, something as simple as a zip archive stored 
on a web server with a known URI can be appropriate. Storage on a file system
is less trustworthy, because it is hard to ensure that the data are never
changed.

One way to support assurance that a dataset is identical to the one previously
used is to store a checksum of the dataset
along side, and to store the checksum somewhere in the project, perhaps even
reporting it in the document. Then, when
reproducing the analysis, it is easy to verify a matching checksum.

The models in this repository are all simple and not trained from any real-world
data, so this project does not demonstrate an approach to managing
irreproducible data that might come from tests, trials or experiments conducted
live in the real world on real systems.

=== Content

The content is anything and everything that is a part of the analysis for the
project. This is likely to include program source code and the text of the
report. The most popular tool for versioning this kind of content is `git`.

Dubbed by its creator as "the stupid content tracker", git is useful for keeping
track of and identifying any collection of files. Any kind of file can be
tracked in a git repository, no complicated software has to be installed to
use it, and no connectivity to an online service is required. In contrast to
science, which is typically done out in the open on purpose, there are other
types of analytical computing that has to be conducted entirely on a corporate
premises, or even across an "air gap", with no internet connectivity of any
kind. Using git to track such work is particularly useful, because it is capable
of reconstructing and reconciling changes, even across an air gap.

Although git can track any kind of content, I believe that reproducibility is
best achieved when only plain text content that was typed by a human be tracked.
With plain text, there is no question as to how it was produced. The author sat
at a text editor or terminal and entered the content. This is in contrast to
content that may have been made by a GUI. Whether the resulting file is binary
or "human-readable" text such as a xml file, it is very difficult to infer
whether the author's intent is truly consistent with the product that arrives
on disk. In the name of pragmatism, I don't believe this principle should always
be kept as a strict rule; but I believe it is a gold standard to strive to. This
example repository will consist only of plan text typed by the author.

To support sharing of the content, plus integration with useful tools like
Continuous Integration and Continuous Deployment pipelines a service such as
GitHub or GitLab can be used. This project uses GitHub with GitHub Actions, and
serves the site on GitHub Pages. This page is build from the GitHub repository
https://github.com/chadsgilbert/ci-data-science[here].

=== Software

Whereas custom code that supports the analysis should be stored as source in
git, it doesn't make sense to track third-party software in the same way.
When using open source software, there are fantastic packaging ecosystems that
can make it easy to install. Proprietary software can be more difficult. In
this repository, only open source software is used. It will use python on
Linux, with third-party libraries. Python's standard package management server,
`pypi`, is used with `pip` to install third-party dependencies.

=== Operating System

The operating system provides the services and libraries required to run the
software, and thus has to be considered in order to support reproducibility.
Virtualization or containerization technologies can both support this to some
extent. How this can all be managed is a large complex topic. In this project,
I sidestep most of that by using GitHub actions, which provides a reliable
environment. It isn't perfect, however. It is conceivalbe that software
updates applied to the virtual machines could change a low level library and
alter the behaviour of my code or other software. Reproducibility is a
matter of degree.

=== Hardware

The hardware that supports the computation here is commodity hardware, a
standard `x86_46` CPU should be quite reliable in producing the same results
*if* we can actually manage to run the same software on it. This is not
something that can always be taken for granted, but it is a reliable assumption
for the kind of analysis I am doing.

== Analysis Pipeline

include::pipeline.adoc[]


